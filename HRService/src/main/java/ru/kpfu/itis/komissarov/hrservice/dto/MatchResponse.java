package ru.kpfu.itis.komissarov.hrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MatchResponse {
    private JobClientResponse jobClientResponse;
    private CVClientResponse cvClientResponse;
}
