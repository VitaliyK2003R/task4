package ru.kpfu.itis.komissarov.hrservice.service;

import ru.kpfu.itis.komissarov.hrservice.dto.CVClientResponse;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.cv.CVResponse;

import java.util.List;

public interface CVService {
    CVClientResponse getCVByActivityType(String activityType);
    List<CVClientResponse> getAll();
}
