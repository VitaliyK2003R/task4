package ru.kpfu.itis.komissarov.hrservice.service;

import ru.kpfu.itis.komissarov.hrservice.dto.JobClientResponse;

public interface JobService {
    JobClientResponse getJobByActivityType(String activityType);
}
