package ru.kpfu.itis.komissarov.hrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobClientResponse {
    private String location;
    private String activityType;
    private String description;
}
