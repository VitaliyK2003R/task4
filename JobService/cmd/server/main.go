package main

import (
	"JobService/internal/config"
	"JobService/internal/repository/mongo"
	"JobService/internal/service"
	"JobService/proto"
	"context"
	"fmt"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"net"
)

var (
	port = "6000"
)

func main() {
	ctx := context.Background()

	err := setupViper()

	tryEnvironmentVariables := false

	if err != nil {
		log.Printf("error reading yml file: %v", err)
		log.Printf("try use environment variables")
		tryEnvironmentVariables = true
	}

	addr := fmt.Sprintf(":%s", port)
	lis, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("error starting tcp listener: %v", err)
	}

	mongoDataBase, err := config.SetupMongoDataBase(ctx, tryEnvironmentVariables)

	if err != nil {
		log.Fatalf("error starting mongo: %v", err)
	}

	jobRepository := mongo.NewJobRepository(mongoDataBase.Collection("jobs"))
	jobService := service.NewJobService(jobRepository)

	grpcServer := grpc.NewServer()

	proto.RegisterJobServiceServer(grpcServer, jobService)

	log.Printf("gRPC stated at %v\n", port)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("error statring gRPC: %v", err)
	}

}

func setupViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
