package service

import (
	"JobService/internal/core"
	"JobService/proto"
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type JobRepository interface {
	GetByActivity(ctx context.Context, activity string) (*core.Job, error)
}

type JobService struct {
	proto.JobServiceServer
	jobRepository JobRepository
}

func NewJobService(jobRepository JobRepository) *JobService {
	return &JobService{
		jobRepository: jobRepository,
	}
}

func (service *JobService) GetJob(ctx context.Context, request *proto.JobRequest) (response *proto.JobResponse, err error) {
	job, err := service.jobRepository.GetByActivity(ctx, request.GetActivityType())

	if err != nil {
		return nil, err
	}

	if job == nil {
		job = &core.Job{
			ID:           primitive.ObjectID{},
			Location:     "",
			ActivityType: "",
			Description:  "",
		}
	}

	return &proto.JobResponse{Location: job.Location, ActivityType: job.ActivityType, Description: job.Description}, nil
}
