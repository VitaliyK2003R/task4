// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v5.26.1
// source: proto/cv.proto

package proto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	CVService_GetCV_FullMethodName  = "/CVService/GetCV"
	CVService_GetAll_FullMethodName = "/CVService/GetAll"
)

// CVServiceClient is the client API for CVService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CVServiceClient interface {
	GetCV(ctx context.Context, in *CVRequest, opts ...grpc.CallOption) (*CVResponse, error)
	GetAll(ctx context.Context, in *EmptyRequest, opts ...grpc.CallOption) (*GetAllResponse, error)
}

type cVServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCVServiceClient(cc grpc.ClientConnInterface) CVServiceClient {
	return &cVServiceClient{cc}
}

func (c *cVServiceClient) GetCV(ctx context.Context, in *CVRequest, opts ...grpc.CallOption) (*CVResponse, error) {
	out := new(CVResponse)
	err := c.cc.Invoke(ctx, CVService_GetCV_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *cVServiceClient) GetAll(ctx context.Context, in *EmptyRequest, opts ...grpc.CallOption) (*GetAllResponse, error) {
	out := new(GetAllResponse)
	err := c.cc.Invoke(ctx, CVService_GetAll_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CVServiceServer is the server API for CVService service.
// All implementations must embed UnimplementedCVServiceServer
// for forward compatibility
type CVServiceServer interface {
	GetCV(context.Context, *CVRequest) (*CVResponse, error)
	GetAll(context.Context, *EmptyRequest) (*GetAllResponse, error)
	mustEmbedUnimplementedCVServiceServer()
}

// UnimplementedCVServiceServer must be embedded to have forward compatible implementations.
type UnimplementedCVServiceServer struct {
}

func (UnimplementedCVServiceServer) GetCV(context.Context, *CVRequest) (*CVResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCV not implemented")
}
func (UnimplementedCVServiceServer) GetAll(context.Context, *EmptyRequest) (*GetAllResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}
func (UnimplementedCVServiceServer) mustEmbedUnimplementedCVServiceServer() {}

// UnsafeCVServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CVServiceServer will
// result in compilation errors.
type UnsafeCVServiceServer interface {
	mustEmbedUnimplementedCVServiceServer()
}

func RegisterCVServiceServer(s grpc.ServiceRegistrar, srv CVServiceServer) {
	s.RegisterService(&CVService_ServiceDesc, srv)
}

func _CVService_GetCV_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CVRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CVServiceServer).GetCV(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CVService_GetCV_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CVServiceServer).GetCV(ctx, req.(*CVRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CVService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EmptyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CVServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CVService_GetAll_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CVServiceServer).GetAll(ctx, req.(*EmptyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// CVService_ServiceDesc is the grpc.ServiceDesc for CVService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CVService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "CVService",
	HandlerType: (*CVServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCV",
			Handler:    _CVService_GetCV_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _CVService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/cv.proto",
}
