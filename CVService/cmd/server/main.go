package main

import (
	"context"
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/VitaliyK2003R/task4/internal/config"
	"gitlab.com/VitaliyK2003R/task4/internal/repository/mongo"
	"gitlab.com/VitaliyK2003R/task4/internal/service"
	"gitlab.com/VitaliyK2003R/task4/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

var (
	port = "5000"
)

func main() {
	ctx := context.Background()

	err := setupViper()

	tryEnvironmentVariables := false

	if err != nil {
		log.Printf("error reading yml file: %v", err)
		log.Printf("try use environment variables")
		tryEnvironmentVariables = true
	}

	addr := fmt.Sprintf(":%s", port)
	lis, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("error starting tcp listener: %v", err)
	}

	mongoDataBase, err := config.SetupMongoDataBase(ctx, tryEnvironmentVariables)

	if err != nil {
		log.Fatalf("error starting mongo: %v", err)
	}

	cvRepository := mongo.NewCVRepository(mongoDataBase.Collection("cvs"))
	cvService := service.NewCVService(cvRepository)

	grpcServer := grpc.NewServer()

	proto.RegisterCVServiceServer(grpcServer, cvService)

	log.Printf("gRPC stated at %v\n", port)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("error statring gRPC: %v", err)
	}

}

func setupViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
