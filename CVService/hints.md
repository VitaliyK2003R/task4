* Сборка приложения

```
go build -o bin/service.exe .\cmd\server\
```

* Переменые окружения

```
export MONGO_URI=mongodb://localhost:27017
export MONGO_NAME=simple_application_database
```

* Скачать Mongo

```
docker pull mongo:4.0.4
```

* Создание network

```
docker network create itis-network
```

* Запуск Mongo в контейнере

```
docker run -d -p 27018:27017 --name mongo-db -v itis-volume:/data/db --network itis-network mongo:latest
```

* Запуск Golang в контейнере

```
docker run --name itis-server-container --network itis-network -e MONGO_NAME=itis_db -e MONGO_URI=mongodb://mongo-db:27017 itis_service_img:latest
```

* Запуск Java в контейнере

```
docker run -d -p 80:8080 --name itis-client-container --network itis-network itis_client_img:latest
```